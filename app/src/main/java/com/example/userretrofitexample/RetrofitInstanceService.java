package com.example.userretrofitexample;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitInstanceService {
    static Retrofit retrofit;
    static String BASE_URL="https://jsonplaceholder.typicode.com/";

    static Retrofit getRetrofitInstance()
    {
    retrofit=new Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    return retrofit;
    }
}
