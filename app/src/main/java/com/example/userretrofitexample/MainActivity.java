package com.example.userretrofitexample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    RecyclerView userRv;
    UserAdapter adapter;
    ArrayList<UserResponse> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        userRv = findViewById(R.id.user_rv);
        arrayList = new ArrayList<>();
        adapter = new UserAdapter(arrayList);
        userRv.setAdapter(adapter);
        getUser();
    }

    private void getUser() {
        ApiInterface apiInterface = RetrofitInstanceService.getRetrofitInstance().create(ApiInterface.class);
        apiInterface.getUserDetails().enqueue(new Callback<List<UserResponse>>() {
            @Override
            public void onResponse(Call<List<UserResponse>> call, Response<List<UserResponse>> response) {
                arrayList.addAll(response.body());
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<UserResponse>> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
